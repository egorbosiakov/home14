////////-------1
let a = {
  b: 5,
  c: 1,
  z: 3,
};
let minKey = "bb";
let min = a[minKey];
for (key in a) {
  if (min > a[key]) {
    min = a[key];
    minKey = key;
  }
}
delete a[minKey];

/////////---------2
let b = {
  ...a,
  f: {
    h: "hi",
    j: 123,
  },
};
console.log(b);
///////////---------3 no
b.f.j = minKey;
//////////----------4
let s = "b" in a;
if (s === true) {
  console.log("da");
} else {
  console.log("no");
}
////////-------------5 ne to

//////////--------2.1
const arr = [1, 3, 23, "mama", { a: 1, b: "2" }];
///////////------2.2
const arrObj = arr[arr.length - 1];
console.log(arrObj);
//////////--------2.3
arr[0] = 3;
arr[1] = 1;
console.log(arr);
///////////---------2.4
const brr = ["ek", 82];
const arrB = {
  ...arr,
  ...brr,
};
console.log(arrB);
//////////////--------2.5 ne gatowo
for (key in arr) {
  if (typeof arr[key] === "Number") {
    arr[key] = arr[key] + 2;
  }
}
////////////----------2.6
const arrNumbers = [2, 56, 23, 1, 2, 1];
//////////---------2.7
let mini = arrNumbers[0];
for (key in arrNumbers) {
  if (mini > arrNumbers[key]) {
    mini = arrNumbers[key];
  }
}
k = arrNumbers.indexOf(mini);
arrNumbers[k] = 99;
console.log(arrNumbers);
/////////----------2.8
/////////----------2.9
const ass = [arrNumbers.splice(1, 3)];
console.log(ass);
